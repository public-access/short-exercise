module.exports = {
  projects: [
    {
      testEnvironment: 'jsdom',
      testMatch: ['**/client/**/*.test.js?(x)'],
    },
    {
      testEnvironment: 'node',
      testMatch: ['**/server/**/*.test.js'],
    },
  ],
};
