import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/store';

import './index.css';
import App from './react/App';
import * as serviceWorker from './serviceWorker';

import socketIOClient from 'socket.io-client';
import { polledDataAction } from './redux/actions';

const calcVariations = (state, data) => {
  if (!state) return data;
  data.instruments.forEach(dataStock => {
    state.instruments.forEach(stateStock => {
      if (stateStock.name === dataStock.name) {
        dataStock.currentPrice.variation =
          dataStock.currentPrice.value - stateStock.currentPrice.value;
      }
    });
  });
};

const socket = socketIOClient(
  `http://localhost:${process.env.REACT_APP_SOCKET_SERVER_PORT}`,
);
socket.on('securities', securities => {
  securities.data.instruments = securities.data.instruments.sort((a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  });
  calcVariations(store.getState().polledData, securities.data);
  store.dispatch(polledDataAction(securities.data));
});

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
