import { combineReducers } from 'redux';
import { aexReducer, stockReducer } from './reducers';

export default combineReducers({
  polledData: aexReducer,
  selectedStock: stockReducer,
});
