const initialAexState = { indexInstrument: {}, instruments: [] };
const initialStockState = {};

export const aexReducer = (state = initialAexState, action) => {
  switch (action.type) {
    case 'POLLED_DATA': {
      return action.payload;
    }
    default: {
      return state;
    }
  }
};

export const stockReducer = (state = initialStockState, action) => {
  switch (action.type) {
    case 'SELECTED_STOCK': {
      return action.payload;
    }
    case 'RESET_SELECTED_STOCK': {
      return {};
    }
    default: {
      return state;
    }
  }
};
