export const polledDataAction = json => {
  return {
    type: 'POLLED_DATA',
    payload: json,
  };
};

export const selectedStockAction = stock => {
  return {
    type: 'SELECTED_STOCK',
    payload: stock,
  };
};

export const resetSelectedStockAction = () => {
  return {
    type: 'RESET_SELECTED_STOCK',
  };
};
