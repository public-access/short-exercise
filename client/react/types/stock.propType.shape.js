import PropTypes from 'prop-types';
export const stockShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  isin: PropTypes.string.isRequired,
  symbol: PropTypes.string.isRequired,
});
