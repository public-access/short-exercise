import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { resetSelectedStockAction } from '../../../redux/actions';
import './StockDetailsContainer.css';
import StockDetails from '../presentation/StockDetails/StockDetails';

class StockDetailsContainer extends React.Component {
  clicked = () => {
    this.props.resetSelectedStock();
  };
  render() {
    if (!this.props.stock.name) {
      return <Redirect to={'/'} />;
    }
    return (
      <div>
        <StockDetails stock={this.props.stock} />
        <button className='margins' onClick={e => this.clicked()}>
          {'back to main page'}
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    stock: state.selectedStock,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetSelectedStock: () => dispatch(resetSelectedStockAction()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StockDetailsContainer);
