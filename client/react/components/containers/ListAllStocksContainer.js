import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectedStockAction } from '../../../redux/actions';
import ListAllStocks from '../presentation/ListAllStocks/ListAllStocks';
class ListAllStocksContainer extends Component {
  render() {
    return (
      <ListAllStocks
        stocks={this.props.instruments}
        selectAction={stock => this.props.showDetails(stock)}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    instruments: state.polledData.instruments,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    showDetails: stock => dispatch(selectedStockAction(stock)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListAllStocksContainer);
