
import React from 'react';
import ReactDOM from 'react-dom';
import ListAllStocks from './ListAllStocks';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ListAllStocks />, div);
  ReactDOM.unmountComponentAtNode(div);
});
