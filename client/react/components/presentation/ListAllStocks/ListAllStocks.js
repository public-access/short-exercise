import React from 'react';
import PropTypes from 'prop-types';
import { stockShape } from '../../../types/stock.propType.shape';
import './ListAllStocks.css';
import { format } from '../../../../utils/formatter';

class ListAllStocks extends React.Component {
  static propTypes = {
    stocks: PropTypes.arrayOf(stockShape),
    selectAction: PropTypes.func.isRequired,
  };
  getCssClass = variation =>
    variation > 0 ? 'positive' : variation < 0 ? 'negative' : '';

  clicked = stock => {
    this.props.selectAction(stock);
  };

  render() {
    const stocks = [];
    if (this.props.stocks) {
      this.props.stocks.forEach(instr => {
        stocks.push(
          <tr
            key={instr.isin}
            onClick={e => this.clicked(instr)}
            className={this.getCssClass(instr.currentPrice.variation)}>
            <td>{instr.isin}</td>
            <td>{instr.name}</td>
            <td>{format(instr.currentPrice.value)}</td>
            <td>{format(instr.currentPrice.variation)}</td>
          </tr>,
        );
      });
    }
    return (
      <div>
        <table className='pure-table'>
          <thead>
            <tr>
              <th>isin</th>
              <th>stock</th>
              <th>curr. value</th>
              <th>variation</th>
            </tr>
          </thead>
          <tbody>{stocks}</tbody>
        </table>
      </div>
    );
  }
}

export default ListAllStocks;
