import React from 'react';
import { stockShape } from '../../../types/stock.propType.shape';

const StockDetails = ({ stock: { name, category, isin, symbol } }) => (
  <div>
    <h2>{`Stock ${name}`}</h2>
    <table className='pure-table'>
      <thead>
        <tr>
          <th> category </th>
          <th> isin </th>
          <th> symbol </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td> {category} </td>
          <td> {isin} </td>
          <td> {symbol} </td>
        </tr>
      </tbody>
    </table>
  </div>
);

StockDetails.propTypes = {
  stock: stockShape,
};

export default StockDetails;
