import React from 'react';
import './Main.css';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import ListAllStocksContainer from '../components/containers/ListAllStocksContainer';
class Main extends React.Component {
  render() {
    if (this.props.redirectTo && this.props.redirectTo.name) {
      return <Redirect to={`/stock/${this.props.redirectTo.name}`} />;
    }
    return (
      <div>
        <h2>Aex Index in Real Time</h2>
        <div className='margins'>
          <ListAllStocksContainer />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    redirectTo: state.selectedStock,
  };
};

export default connect(mapStateToProps)(Main);
