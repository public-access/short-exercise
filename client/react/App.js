import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Main from './pages/Main';
import Details from './pages/Details';
import NotFound from './pages/NotFound';

class App extends React.Component {
  render() {
    return (
      <Router basename='aex'>
        <Switch>
          <Route path='/' exact component={Main} />
          <Route path='/stock/:name' exact component={Details} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App;
