const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'EUR',
  minimumFractionDigits: 6,
});
export const format = value => {
  if (value) {
    return formatter.format(value);
  }
};
