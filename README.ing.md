## Assesment for Investments

For this assesment you will be asked to build a web application to display some stock exchange endpoint.
The goal is to deliver a working application that will show the available stocks and their price in a list.

### Polling
The endpoint should be polled, and after each new dataset is retrieved the list should highlight which instruments have increased in price,
and which instruments have decreased in price in comparison with the previous dataset.

### Navigation
On selecting a stock item from the list, a new view should be pushed where some more detailed information about the stock item is displayed.

### Networking
The endpoint to use is [here](https://services.ing.nl/api/securities/mobile/markets/stockmarkets/AEX).
If this endpoint is not reachable from your standard browser use a REST client Postman.

### JSON
Looking at the response, the list of instruments should be used as the datasource:

```
"instruments": [
        {
            "instrumentType": "STOCK",
            "exchange": "Equiduct Netherlands",
            "category": "Aandelen",
            "currency": "EUR",
            "isin": "NL0000009355",
            "name": "Unilever",
            "symbol": "UN",
            "uid": "7b2273796d626f6c223a22554e227d",
            "currentPrice": {
                "value": 49.055,
                "unit": "EUR",
                "percent": false
            },
            "priceDetails": {
                "closePrice": {
                    "value": 49.02,
                    "unit": "EUR",
                    "percent": false
                },
                "highPrice": {
                    "value": 49.255,
                    "unit": "EUR",
                    "percent": false
                },
                "lowPrice": {
                    "value": 48.78,
                    "unit": "EUR",
                    "percent": false
                },
                "openPrice": {
                    "value": 49.0699,
                    "unit": "EUR",
                    "percent": false
                }
            },
            "intradayPriceMutation": 0.07139942880456956,
            "datetime": "2019-02-20T15:15:39.739Z"
        },
        {...}
        ]
```

In particular, the `name` can be used for the name of the instrument, and the `currentPrice.value` for the price.
For the pushed detail view, you are free to display any additional fields.

### Assignment
In sum, the following features should be present in your app:

- [ ] List of all instruments is the initial view of the app
- [ ] Poll the endpoint and show increasers and decreasers compared to the previous dataset
- [ ] On selecting an instrument, push a view with more instrument details

The code can be pushed to this repository.
