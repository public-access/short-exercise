## About

This application monitors ING security API and displays the current value of AEX stocks in real time.
The app has two parts, a nodejs back-end server running websocket.io and a react front-end client listening to web socket messages.
The back-end polls ING service bypassing CORS.
React uses a Redux store to coordinate the content of two pages, a main page that lists all stocks and a details page that shows additional info for a selected stock.

## Prerequisites

A recent version of nodejs and npm installed globally in your machine
Access to the internet to link to purecss CDN server (not mandatory but nice for a more nicely formatted table)
The app will by default use the following ports:
- back-end: 4001
- front-end: 3005

## Installation

- Clone the project
``` git clone https://gitlab.com/public-access/short-exercise.git ```
- Install dependencies
``` npm i ```
- review .env settings in the root folder and change if necessary
Note: just a limited set of settings has been exposed in .env for customization. For example, both client and server assume 'localhost' (create-react-app hides its start script deep down in node_modules and we did not want to spend time flexibilizing this setup)
§ 
## Running

You can quickly get the app started:
``` npm run start ```
It will get both the back-end and the front-end started.
You can check if the API polling is working in the console.
You can check the back-end server in the browser via the health check page:
``` http://localhost:<4001>//healthCheck ```
The front-end page should open automatically in your default browser

## Running the optimized front-end build

create-react-app provides out-of-the-box webpack configuration and build optimization for react apps.
We took advantage of this setup (apart minor changes in the internal scripts that allowed us to modify the folder structure of the app).
If you run
``` npm run build ```
Then you can serve the front-end using your preferred http server.
The default port is 8000.
But you also need to start the back-end manually:
``` node ./server/app.js ```

## Tests

The following command will run back-end tests and front-end tests with test coverage report
``` npm run test ```
