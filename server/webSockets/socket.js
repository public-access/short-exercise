let socketIO;
const poller = require('./data/asyncPolling');

const socket = {
  init: server => {
    socketIO = require('socket.io')(server);
    socketIO.on('connection', socket => {
      console.log('New client connected');
      socket.on('disconnect', () => {
        poller.stop();
        console.log('Client disconnected');
        process.exit();
      });
      poller.start(emit);
    });
  },
};

const emit = async () => {
  const stockMarkets = await require('./data/stockMarkets').fetchSecurities();
  socketIO.emit('securities', { data: stockMarkets });
};

module.exports = socket;
