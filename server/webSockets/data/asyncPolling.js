const events = require('events');
const eventsEmitter = new events.EventEmitter();
const timeout = process.env.POLLING_CYCLE_MS || 5000;

let timer;

eventsEmitter.on(
  'setTimer',
  fn => (timer = setTimeout(() => asyncPolling.start(fn), timeout)),
);

/*
  two techniques to set the timer: recursive call and event handler (current)
  both ways work
  chose events to prevent memory leaks in the execution context of the memory stack
*/
const asyncPolling = {
  start: async fn => {
    await fn();
    console.log(
      `new aex data polled on ${new Date()}
      next polling in ${timeout} milliseconds`,
    );
    eventsEmitter.emit('setTimer', fn);
  },
  stop: () => {
    clearTimeout(timer);
    console.log('polling terminated');
  },
};

module.exports = asyncPolling;
