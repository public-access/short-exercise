const axios = require('axios');

const stockMarkets = {
  fetchSecurities: () =>
    axios
      .get(process.env.POLLING_URL)
      .then(res => res.data)
      .catch(err => console.log(err)),
  heathyCheck: async () => {
    const { indexInstrument } = await stockMarkets.fetchSecurities();
    return console.log(`new Date() ${indexInstrument.currentPrice.value}`);
  },
};

module.exports = stockMarkets;
