const asyncPolling = require('./asyncPolling');
const axios = require('axios');
const stockMarkets = require('./stockMarkets');

jest.mock('axios');
it('should repeatedly poll the remote endpoint', () => {
  asyncPolling.start(stockMarkets.heathyCheck);
  expect(axios.get).toBeCalled();
});
