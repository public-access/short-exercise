const fetchSecurities = require('./stockMarkets').fetchSecurities;

it('should retrieve stock markets data', async () => {
  const result = await fetchSecurities();
  expect(result).toBeDefined();
});
