const router = require('express').Router();

const routes = {
  healthCheck: router.get('/healthCheck', (req, res) =>
    res.send('I am alive').status(200),
  ),
};

module.exports = routes;
