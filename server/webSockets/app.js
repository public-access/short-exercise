const express = require('express');
const http = require('http');
require('dotenv').config();

const app = express();
const routes = require('./routes');
app.use(routes.healthCheck);

const server = http.createServer(app);
const port = process.env.SOCKET_SERVER_PORT || 4001;
server.listen(port, () => console.log(`Listening on port ${port}`));

require('./socket').init(server);
