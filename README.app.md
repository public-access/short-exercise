## Project Structure

We use folder names as a way of giving an overall view of architecture and techonology choices.
``` server/websockets
    client/react
```

## Client and Server

These two folders could eventually be split up in two truly independent packages.

For example, we might create distinct package.json in each folder for managing non-shared dependencies and use yarn workspaces to manage shared dependencies.

Going further a tool like lerna might be used to manage and deploy these two packages independently.

At the moment these folders are still coupled, but that is fine. Additional steps like the ones mentioned above should be driven by business requirements, such as extending and reusing each package as components of other applications.

## Tests and Error Handling

In a real case scenario way more tests should be implemented. For example, it would be nice to use a tool like json-server to simulate api endpoint and check the whole polling cycle more throughly. Time limitations prevented that, but the idea is in the air.

Test coverage is set up and can be triggered by
``` npm run test:cover```

Another additional improvement would be to enforce coverage thresholds during the build phase.

This app writes some log messages here and there as a way of providing clues on issues and status. A real case scenario might require a more sophisticated logging mechanism, suitable for app live monitoring, or maybe more status checking endpoints.

A more comprehensive review of error situations and correspondent handlers should be done, which might lead to the introduction of things like React's Error Boundaries in the front-end.

## Server

Server tests should not make use of jsdom, otherwise an OPTIONS pre-flight request will be issued and the api poll will fail due to CORS validation.
Therefore there a .jest.config.js file sets up 'node' tests in this part of the application.

Other than that all files are named with in an 'intention revealing' mode:
- app.js is the main back-end file
- routes.js sets up espress routes (actually, only one, '/healthCheck')
- socket.js creates the socket server, starts the poller and emits the json results

The data/ folder contains the poller and the api call.
At the end of an api call a timer is created to keep refreshing the json content

Once the websocket server is start it starts immediately polling and emitting results; a small improvement would be to listen to a client message saying "I am ready to receive" and only then start the polling-messaging cycle.

## Client

index.js is a bit laborious here. Besides mounting the app in the DOM is also instantiates the websocket client and format the payload of the api (enforcing alphabetic sorting and calculating price changes).

An obvious improvement would be to decouple these things in their individual modules. For the sake of simplicity we leave it as is in this initial version.

The main client folders provide hints on what the app is:
- react
- redux
- static
- utils

Static contains index.html, images, etc.

The react folder is split up as follows:
- App.js - the application routes (basename is '/aex')
- components/
  - containers/ - components responsible for redux bindings
  - presentational - the UI components, dedicated only to html and css
- pages/ - all landing pages including 404

The redux store is structured in the usual way: actions, reducers, rootReducer, etc. We have not create an action types to centralize a list of valid action names,that is to be improved in version 2.

The store contains two main sections:
polled data: the json received from the api, having the current priced spiced up with the price variation between calls
selected stock: the stock clicked in the main page to be shown in the details page

Notice that we use a simple component breakdown, good enough for the time being. In future iterations the Main page component, ListAllStocks, might be enhanced, so that each row in the table is a separate component, therefore allowing finer control over re-rendering - which might require a re-design of the store or the usage of memoized selectors retrieving data at stock level, not, as it is now, as a single lump of data.
